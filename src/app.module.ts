import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {ConfigModule} from "@nestjs/config";
import configuration from './config/configuration'
import {DatabaseModule} from "@app/database";

@Module({
  imports: [
      ConfigModule.forRoot({
        load: [configuration],
        isGlobal: true
      }),
      DatabaseModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
