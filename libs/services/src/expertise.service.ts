import {BaseService} from "@app/base";
import {Expertise} from "@app/schemas";
import {InjectModel} from "nestjs-typegoose";
import {ReturnModelType} from "@typegoose/typegoose";
import {Injectable} from "@nestjs/common";

@Injectable()
export class ExpertiseService extends BaseService<Expertise> {
    constructor(
        @InjectModel(Expertise) protected model: ReturnModelType<typeof Expertise>
    ) {
        super(model);
    }
}