import {BaseService} from "@app/base";
import {InjectModel} from "nestjs-typegoose";
import {Experience} from "@app/schemas/experience";
import {ReturnModelType} from "@typegoose/typegoose";
import {Injectable} from "@nestjs/common";

@Injectable()
export class ExperienceService extends BaseService<Experience> {
    constructor(
        @InjectModel(Experience) protected model: ReturnModelType<typeof Experience>
    ) {
        super(model);
    }
}