import {BaseService} from "@app/base";
import {User} from "@app/schemas";
import {InjectModel} from "nestjs-typegoose";
import {ReturnModelType} from "@typegoose/typegoose";
import {Injectable} from "@nestjs/common";

@Injectable()
export class AuthService extends BaseService<User> {
    constructor(
        @InjectModel(User) protected model: ReturnModelType<typeof User>
    ) {
        super(model);
    }
}