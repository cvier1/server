import {BaseService} from "@app/base";
import {User} from "@app/schemas";
import {Injectable} from "@nestjs/common";
import {InjectModel} from "nestjs-typegoose";
import {ReturnModelType} from "@typegoose/typegoose";

@Injectable()
export class UserService extends BaseService<User> {
    constructor(
        @InjectModel(User) protected model: ReturnModelType<typeof User>
    ) {
        super(model)
    }
}