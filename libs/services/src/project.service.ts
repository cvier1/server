import {BaseService} from "@app/base";
import {Project} from "@app/schemas";
import {InjectModel} from "nestjs-typegoose";
import {ReturnModelType} from "@typegoose/typegoose";
import {Injectable} from "@nestjs/common";

@Injectable()
export class ProjectService extends BaseService<Project> {
    constructor(
        @InjectModel(Project) protected model: ReturnModelType<typeof Project>
    ) {
        super(model);
    }
}