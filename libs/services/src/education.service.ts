import {BaseService} from "@app/base";
import {Education} from "@app/schemas";
import {InjectModel} from "nestjs-typegoose";
import {ReturnModelType} from "@typegoose/typegoose";
import {Injectable} from "@nestjs/common";

@Injectable()
export class EducationService extends BaseService<Education> {
    constructor(
        @InjectModel(Education) protected model: ReturnModelType<typeof Education>
    ) {
        super(model);
    }
}