import {BaseService} from "@app/base";
import {Injectable} from "@nestjs/common";
import {Profile} from '@app/schemas'
import {InjectModel} from "nestjs-typegoose";
import {ReturnModelType} from "@typegoose/typegoose";

@Injectable()
export class ProfileService extends BaseService<Profile> {
    constructor(
        @InjectModel(Profile) protected model: ReturnModelType<typeof Profile>
    ) {
        super(model);
    }
}