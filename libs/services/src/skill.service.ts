import {BaseService} from "@app/base";
import {Skill} from "@app/schemas";
import {InjectModel} from "nestjs-typegoose";
import {ReturnModelType} from "@typegoose/typegoose";
import {Injectable} from "@nestjs/common";

@Injectable()
export class SkillService extends BaseService<Skill> {
    constructor(
        @InjectModel(Skill) protected model: ReturnModelType<typeof Skill>
    ) {
        super(model);
    }
}