import {BaseService} from "@app/base";
import {External} from "@app/schemas";
import {InjectModel} from "nestjs-typegoose";
import {ReturnModelType} from "@typegoose/typegoose";
import {Injectable} from "@nestjs/common";

@Injectable()
export class ExternalService extends BaseService<External> {
    constructor(
        @InjectModel(External) protected model: ReturnModelType<typeof External>
    ) {
        super(model);
    }
}