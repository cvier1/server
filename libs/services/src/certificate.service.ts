import {BaseService} from "@app/base";
import {Certificate} from "@app/schemas";
import {InjectModel} from "nestjs-typegoose";
import {Injectable} from "@nestjs/common";

@Injectable()
export class CertificateService extends BaseService<Certificate> {
    constructor(
        @InjectModel(Certificate) protected model: ReturnType<typeof Certificate>
    ) {
        super()model;
    }
}