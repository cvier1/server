import {BaseSchema} from "@app/base/base.schema";
import {User} from "@app/schemas/user.schema";
import {prop} from "@typegoose/typegoose";

export class Skill extends BaseSchema<User> {

    @prop({ required: true })
    name: string

    @prop()
    percentage: number

    @prop({ default: 0 })
    order: number

}