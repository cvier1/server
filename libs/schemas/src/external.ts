import {BaseSchema} from "@app/base/base.schema";
import {User} from "@app/schemas/user.schema";
import {prop} from "@typegoose/typegoose";

export class External extends BaseSchema<User> {

    @prop({ required: true })
    name: string

    @prop()
    description: string

    @prop()
    link: string

    @prop({ default: 0 })
    order: number

}