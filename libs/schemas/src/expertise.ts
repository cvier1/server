import {BaseSchema} from "@app/base/base.schema";
import {User} from "@app/schemas/user.schema";
import {prop} from "@typegoose/typegoose";

export class Expertise extends BaseSchema<User> {

    @prop({ required: true })
    name: string

    @prop({ default: 0 })
    order: number

}