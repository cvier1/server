import {BaseSchema} from "@app/base/base.schema";
import {User} from "@app/schemas/user.schema";
import {prop} from "@typegoose/typegoose";

export class Experience extends BaseSchema<User> {

    @prop({ required: true })
    designation: string

    @prop({ required: true })
    company: string

    @prop()
    address: string

    @prop({ required: true })
    started: Date

    @prop()
    ended: Date

    @prop({ required: true })
    isEnded: boolean

    @prop({ default: 0 })
    order: number

}