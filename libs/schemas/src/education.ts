import {BaseSchema} from "@app/base/base.schema";
import {User} from "@app/schemas/user.schema";
import {prop} from "@typegoose/typegoose";

export class Education extends BaseSchema<User> {

    @prop({ required: true })
    degree: string

    @prop({ required: true })
    institute: string

    @prop()
    address: string

    @prop({ required: true })
    started: Date

    @prop()
    ended: Date

    @prop()
    isEnded: boolean

    @prop({ default: 0 })
    order: number

}