import {BaseSchema} from "@app/base/base.schema";
import {User} from "@app/schemas/user";
import {prop} from "@typegoose/typegoose";

export class Certificate extends BaseSchema<User> {

    @prop({required: true})
    title: string

    @prop()
    description: string

    @prop({default: 0})
    order: number

}