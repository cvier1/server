import {BaseSchema} from "@app/base/base.schema";
import {User} from "@app/schemas/user.schema";
import {prop} from "@typegoose/typegoose";

export class Project extends BaseSchema<User> {

    @prop({required: true})
    title: String

    @prop()
    description: String

    @prop()
    skilles: [Skill]

    @prop({default: 0})
    order: number
}