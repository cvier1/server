import {prop} from "@typegoose/typegoose";
import {BaseSchema} from "@app/base/base.schema";
import {IsEmail, IsNotEmpty} from "class-validator";

export class User {

    @prop({ default:  true })
    active: boolean

    @prop({ default:  false })
    deleted: boolean

    @prop({ type: String, unique: true })
    @IsNotEmpty()
    @IsEmail()
    email: string

    @prop({type: String})
    @IsNotEmpty()
    password: string

}