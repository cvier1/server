import {BaseSchema} from "@app/base/base.schema";
import {prop} from "@typegoose/typegoose";
import {User} from "@app/schemas/user.schema";

export class Profile extends BaseSchema<User>  {

    @prop({ required: true })
    firstName: string

    @prop({ required: false })
    middleName: string

    @prop({ required: false })
    lastName: string

    @prop({ required: false })
    email: string

    @prop({ required: false })
    phone: string

    @prop({ required: false })
    designation: string

    @prop({ required: false })
    photo: string

    @prop({ required: false })
    website: string

    @prop({ required: false })
    skype: string

    @prop({ required: false })
    summary: string
}