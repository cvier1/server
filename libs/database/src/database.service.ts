import { Injectable } from '@nestjs/common';
import {ConfigService} from "@nestjs/config";
import {TypegooseModuleOptions} from "nestjs-typegoose";

@Injectable()
export class DatabaseService {
    constructor(
        private config: ConfigService
    ) {}

    createTypegooseOptions(): TypegooseModuleOptions {
        return {
            uri: this.config.get('database.uri'),
            useNewUrlParser: true,
            useCreateIndex: true,
            useFindAndModify: false,
            useUnifiedTopology: true,
        }
    }
}
