import {DynamicModule, Module} from '@nestjs/common';
import { DatabaseService } from './database.service';
import {TypegooseModule} from "nestjs-typegoose";
import {TypegooseClass, TypegooseClassWithOptions} from "nestjs-typegoose/dist/typegoose-class.interface";

@Module({
  imports: [
      TypegooseModule.forRootAsync({
        useClass: DatabaseService
      })
  ],
  providers: [DatabaseService],
  exports: [DatabaseService],
})
export class DatabaseModule {
  static feature(
      feature: (TypegooseClass | TypegooseClassWithOptions)[]
  ): DynamicModule {
    return TypegooseModule.forFeature(feature)
  }
}
