import {ReturnModelType} from "@typegoose/typegoose";
import {Types} from "mongoose";

export abstract class BaseService<Schema> {
    constructor(
        protected model: ReturnModelType<any>
    ) {}

    async getById(
        id: string
    ): Promise<Schema> {
        return await this.model.findOne({
            _id: Types.ObjectId(id)
        }).exec()
    }

    async all(
        filters: any
    ): Promise<Schema[]> {
        return this.model.find(filters).exec()
    }

    async edit(
        data: any,
        id?: string
    ): Promise<Schema> {
        if (id) {
            return await this.model.findOneAndUpdate(
                { _id: Types.ObjectId(id) },
                data
            ).exec()
        } else {
            return await this.model.create(data)
        }
    }

    async remove(
        id: string
    ): Promise<Schema> {
        return await this.model.findOneAndRemove(
            { _id: Types.ObjectId(id) }
        ).exec()
    }
}
