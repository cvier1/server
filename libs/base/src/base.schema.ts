import {prop, Ref} from "@typegoose/typegoose";
import {Types} from "mongoose";

export class BaseSchema<User> {

    @prop({ default:  true })
    active: boolean

    @prop({ default:  false })
    deleted: boolean

    @prop({ required: true, ref: 'User' })
    owner: Ref<User>

}